/**
 *------------------------------------------------------------------------------------------------------
 * 																									   |
 *                                      CREACION DE LA BASE DE DATOS								   | 
 *      										                                                       |
 *      FECHA:       8 Enero 2020                                                                      |
 *      DESCRIPCION: Se crea una base de datos, para guardar la informacion necesaria para el          |
 *      			 funcionamiento basico de la aplicacion      									   |
 *      NOM CLASE:   El nombre de la clase principal es baseDatos                                      |
 *_____________________________________________________________________________________________________| */


function cls_db() {
    // Constantes DB
    var namedb = "db_appCidenet"; //"DBApp7"; //Nombre de la base de datos
    var version = "1.0"; //Version 
    var descrip = "Base de datos aplicacion localizacion"; //Descripcion 
    var tam = 2 * 1024 * 1024; //Tamano

    this.sqlquery = "";
    //Crear conexion con la base de datos
    this.conexion = function () {

        this.db = window.openDatabase(namedb, version, descrip, tam);

    };

    //Conexion con la base de datos, metodo global
    this.conexion_global = function () {

        this.conexion(); //Se invoca el metodo conexion
        return this.db; //Al realizar un return, se obtendra el valor publicamente desde otra clase

    };
    this.ejemplo = function () {
        console.log("entro");
    };

    //Insertar datos 
    this.insertarDatosInfo = function (PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, fecha) {
        this.conexion(); //Se invoca el metodo conexion
        //Una vez invocado, se puede acceder a la variable de conexion this.db
        
        this.db.transaction(createquery, ErrorDB, ExitoDB);

        function createquery(tx) {
            var query = "INSERT OR IGNORE INTO tbInformacion(PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, fecha) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
            tx.executeSql(query, [PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, fecha]);
        }

        function ErrorDB(e) {
            console.log("Error al guardar datos de Informacion");
        }

        function ExitoDB() {
            console.log("Exito al guardar datos info...");
            swal({
               title: "El registro fue creado!",
               text: "",
               type: "success",
               showCancelButton: false,
               confirmButtonColor: "#3085d6",
               confirmButtonText: "Aceptar",
               closeOnConfirm: false}, 
            function(){ 
               window.location.href = "index.html";
            });
        }
    }

    //Actualizar datos 
    this.actualizarDatosInfo = function (PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, fecha, id) {
        this.conexion(); //Se invoca el metodo conexion
        //Una vez invocado, se puede acceder a la variable de conexion this.db
        debugger;
        this.db.transaction(createquery, ErrorDB, ExitoDB);

        function createquery(tx) {
            var query = "UPDATE tbInformacion SET PrimerApellido=?, SegundoApellido=?, PrimerNombre=?, OtroNombre=?, Pais=?, TipoID=?, NumID=?, Email=?, FechIng=?, Area=?, Estado=?, fecha=? WHERE id = ?";
            tx.executeSql(query, [PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, fecha, id]);
        }

        function ErrorDB(e) {
            console.log("Error al guardar datos de Informacion");
        }

        function ExitoDB() {
            console.log("Exito al guardar datos info...");
            swal({
               title: "El registro fue editado!",
               text: "",
               type: "success",
               showCancelButton: false,
               confirmButtonColor: "#3085d6",
               confirmButtonText: "Aceptar",
               closeOnConfirm: false}, 
            function(){ 
               window.location.href = "index.html";
            });
        }
    }

    //Crear estructura de la base de datos
    this.crear_db = function () {

        this.conexion(); //Se invoca el metodo conexion
        //Una vez invocado, se puede acceder a la variable de conexion this.db
        this.db.transaction(createquery, ErrorDB, ExitoDB);

        function createquery(tx) {

            tx.executeSql("CREATE TABLE IF NOT EXISTS tbInformacion(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "PrimerApellido char(100)," +
                "SegundoApellido char(100)," + 
                "PrimerNombre char(100)," +
                "OtroNombre char(100)," +
                "Pais char(100)," +
                "TipoID char(100)," +
                "NumID char(100)," +
                "Email char(100)," +
                "FechIng char(100)," +
                "Area char(100)," +
                "Estado char(100)," +    
                "fecha char(100)," +
                "UNIQUE(id));"
            );
        }

        function ErrorDB(e) {
            console.log("Error al crear la estructura de la base de datos");
        }

        function ExitoDB() {
            console.log("Exito al crear la estructura de la base de datos");
        }
    };

    // ********************
    // *CONSULTA DE DATOS *
    // ********************

    this.GetDataInfo = function (callback) {
        this.conexion();
        this.db.transaction(consultaUrl, errorDB, exitoDB);
        debugger;
        function consultaUrl(tx) {
            //Setencia sql
            tx.executeSql("SELECT id, PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, fecha FROM tbInformacion", [], mostrar, errorDB);

            function mostrar(tx, results) {
                var tamano = results.rows.length;
                if (tamano > 0) {
                    //Callback con el resultado obtenido del select
                    callback(results);
                } else {
                    console.log("Ejecucion correcta: Empleado. No se encontro datos");
                }
            };
        };
        //Validacion y mensajes de error
        function errorDB(e) {
            console.log("Error al consultar Empleado");
        }

        function exitoDB() {
            console.log("Exito al consultar Empleado");

        }     

    };

    this.DeleteDatabase = function () {
        this.conexion(); //Se invoca el metodo conexion
        //Una vez invocado, se puede acceder a la variable de conexion this.db
        this.db.transaction(createquery, ErrorDB, ExitoDB);

        function createquery(tx) {

            tx.executeSql("DROP TABLE tbInformacion");
        }

        function ErrorDB(e) {
            console.log("Error al guardar datos de empleado");
        }

        function ExitoDB() {
            console.log("Exito al guardar datos de empleado");
            window.location = "index.html";
        }
    };

    this.DeleteDataReg = function (id) {
        debugger;
        this.conexion(); //Se invoca el metodo conexion
        //Una vez invocado, se puede acceder a la variable de conexion this.db
        this.db.transaction(createquery, ErrorDB, ExitoDB);

        function createquery(tx) {

            // tx.executeSql("DROP TABLE tbInformacion");
            // tx.executeSql("UPDATE tbInformacion SET  WHERE id = ?", [id], ExitoDB, ErrorDB);
            tx.executeSql("DELETE FROM tbInformacion WHERE id = ?", [id], ExitoDB, ErrorDB);
        }

        function ErrorDB(e) {
            console.log("Error al guardar datos de empleado");
        }

        function ExitoDB() {
            console.log("Exito al guardar datos de empleado");
            window.location = "index.html";
        }
    }

   


}