/**
 *------------------------------------------------------------------------------------------------------
 *                                                                                                     |
 *                                      CREACION DEL CONTROLADOR                                       | 
 *                                                                                                     |
 *      FECHA:       8 Enero 2020                                                                      |
 *      DESCRIPCION: Se crea un controlador para llevar a cabo las funcionalidades principales tales   |
 *                   como mannejo DB con WebSQL, popup, validaciones, entre otros                      |
 *                   funcionamiento basico de la aplicacion                                            |
 *_____________________________________________________________________________________________________| */

var app = angular.module('CidenetPrueba',[ ]);

    
app.controller('listadoCtrl', ['$scope', function($scope){    
 
    $scope.posicion = 10;

    
    $scope.siguiente = function(){

        if ($scope.personas.length > $scope.posicion ) {

            $scope.posicion += 10;
        }

    }

    $scope.anterior = function(){

        if ($scope.posicion > 10 ) {

            $scope.posicion -= 10;
        }

    }

    $scope.FechaActual = function(){

        //Fecha actual
        var actual = new Date();
        $scope.CurrentDate = actual;
        console.log($scope.CurrentDate);

        //Fecha actual formato ISO
        var today = new Date().toISOString().split('T')[0];
        $scope.today = today;
        console.log($scope.today);

        //Validacion Fecha Ingreso
        var month = actual.getMonth() + 1;
        var day = actual.getDate();
        var year = actual.getFullYear();
        if(month === 1){
            month = 12;
            year = year - 1;
        }
        else{
            month = month - 1;
            month = '0' + month.toString();
        }        
        day = '0' + day.toString();
        
        var minDate = year + '-' + month + '-' + day;
        $scope.minDate = minDate;
        console.log(minDate);
    }
   
    $scope.GenEmail = function(){

        debugger;
        var PrimerApellido   = document.getElementById("PrimerApellido").value;
        var SegundoApellido = document.getElementById("SegundoApellido").value;
        var dominioCol = "@cidenet.com.co";
        var dominioUSA = "@cidenet.com.us";

        if(document.getElementById("Pais").value = "Colombia")
        {
            var Email = dominioCol;
        }
        else{
            var Email = dominioUSA;
        }

        $scope.Email = Email;
        $scope.$apply();

    } 


    $scope.formVisibilityReg = false;
    $scope.formVisibilityEdit = false;

    $scope.ShowFormReg = function(){

        $scope.formVisibilityReg = true;
        console.log($scope.formVisibilityReg);
        $scope.ShowBtnCrear();

    } 

    $scope.ShowFormEdit = function(){

        $scope.formVisibilityEdit  = true;
        console.log($scope.formVisibilityEdit);
        $scope.ShowBtnCrear();

    }

    $scope.ShowBtnCrear = function(){

        if($scope.formVisibilityReg === true || $scope.formVisibilityEdit === true){
            $scope.formVisibility  = true;      
        }
        console.log($scope.formVisibility)

    }  

    //Crear instancia Base de datos
    var db = new cls_db();
    db.conexion_global();
    $scope.date = new Date();
    db.crear_db();

    // Guardar informacion offline
    $scope.SaveInfo = function () {  

        debugger;
        var PrimerApellido   = document.getElementById("PrimerApellido").value;
        var SegundoApellido = document.getElementById("SegundoApellido").value;
        var PrimerNombre = document.getElementById("PrimerNombre").value;
        var OtroNombre = document.getElementById("OtroNombre").value;
        var Pais = document.getElementById("Pais");
        Pais = Pais.options[Pais.selectedIndex].text;
        var TipoID = document.getElementById("TipoID");
        TipoID = TipoID.options[TipoID.selectedIndex].text;
        var NumID  = document.getElementById("NumID").value;
        var Email  = document.getElementById("Email").value;
        var FechIng  = document.getElementById("FechIng").value;
        var Area = document.getElementById("Area");
        Area = Area.options[Area.selectedIndex].text;
        var Estado = document.getElementById("Estado");
        Estado = Estado.options[Estado.selectedIndex].text;
        var FechReg = document.getElementById("FechReg").value;

        $scope.formVisibility = false;
        console.log($scope.formVisibility)

        var flag = $scope.validateCampo(PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, FechReg);

        if (flag) {

            //Creacion de registros en WebSQL
            db.insertarDatosInfo(PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, FechReg);
            // swal("Exito!", "El registro fue creado!", "success");
        }
        else{
            console.log("Error!", "Formulario incompleto.");
            swal("Error!", "Formulario incompleto.", "error");
        }
        
    }

    // Guardar informacion offline
    $scope.SaveInfoEdit = function () {  

        debugger;
        var PrimerApellido   = document.getElementById("PrimerApellidoEdit").value;
        var SegundoApellido = document.getElementById("SegundoApellidoEdit").value;
        var PrimerNombre = document.getElementById("PrimerNombreEdit").value;
        var OtroNombre = document.getElementById("OtroNombreEdit").value;
        var Pais = document.getElementById("PaisEdit");
        Pais = Pais.options[Pais.selectedIndex].text;
        var TipoID = document.getElementById("TipoIDEdit");
        TipoID = TipoID.options[TipoID.selectedIndex].text;
        var NumID  = document.getElementById("NumIDEdit").value;
        var Email  = document.getElementById("EmailEdit").value;
        var FechIng  = document.getElementById("FechIngEdit").value;
        var Area = document.getElementById("AreaEdit");
        Area = Area.options[Area.selectedIndex].text;
        var Estado = document.getElementById("EstadoEdit");
        Estado = Estado.options[Estado.selectedIndex].text;
        var FechReg = document.getElementById("FechRegEdit").value;
        var id = $scope.registros[0].id;

        $scope.formVisibility = false;
        console.log($scope.formVisibility)

        var flag = $scope.validateCampo(PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, FechReg, id);

        
        if (flag) {

            //Creacion de registros en WebSQL
            db.actualizarDatosInfo(PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, FechReg, id);
        }
        else{

            console.log("Error!", "Formulario incompleto.");
            swal("Error!", "Formulario incompleto.", "error");
        }
        
    }

    $scope.validateCampo = function(PrimerApellido, SegundoApellido, PrimerNombre, OtroNombre, Pais, TipoID, NumID, Email, FechIng, Area, Estado, FechReg){

        var flag = true;

        if (PrimerApellido === "") {
            flag = false;
        }else if (SegundoApellido === "") {
            flag = false;
        }else if(PrimerNombre === ""){
            flag = false;
        }else if(OtroNombre === ""){
            flag = false;
        }else if(Pais === ""){
            flag = false;
        }
        else if(TipoID === ""){
            flag = false;
        }
        else if(NumID === ""){
            flag = false;
        }
        else if(Email === ""){
            flag = false;
        }
        else if(FechIng === ""){
            flag = false;
        }
        else if(Area === ""){
            flag = false;
        }
        else if(Estado === ""){
            flag = false;
        }

        return flag;
    }

    //Consulta de registros en WebSQL
    $scope.getinfo = function(){
        //Obtenemos informacion de la base datos local.
        db.GetDataInfo(function (datos) {
            if (datos.rows != null) {
                // Datos de cabecera
                console.log('Guardando datos de informacion final.');
                $scope.SaveDataInfo(datos.rows);
            }
        });                
    }

    //Editar de registros en WebSQL
    $scope.Editar = function(persona) {
            

        debugger;      
        $scope.ShowFormEdit();
        console.log(persona);
        var informacion = [];
        var info = {};
        // document.getElementById('PaisEdit').value = "@cidenet.com.us";
        // document.getElementById("PaisEdit");
        // var Pais = "@cidenet.com.us";
        // Pais.options[Pais.selectedIndex].text = Pais;

         // if(persona.Pais === "Estados Unidos"){
         //    document.getElementById("PaisEdit") = Pais; 
         // }

        info['id']              = persona.id
        info['PrimerApellido']  = persona.PrimerApellido
        info['SegundoApellido'] = persona.SegundoApellido
        info['PrimerNombre']    = persona.PrimerNombre
        info['OtroNombre']      = persona.OtroNombre
        // info['Pais']            = persona.Pais
        // info['TipoID']          = persona.TipoID
        info['NumID']           = persona.NumID
        info['Email']           = persona.Email
        info['FechIng']         = persona.FechIng
        // info['Area']            = persona.Area
        // info['Estado']          = persona.Estado
        info['FechReg']         = persona.FechReg
        informacion.push(info);
        info = {};   
        

        $scope.registros = informacion;
        $scope.$apply(); //Aplica obligatoriamente los cambios en el refresh de la pagina

        
    }

    //Llevar registros al DOM
    $scope.SaveDataInfo = function(data){

        //Armar objeto y enviar.
        var informacion = [];
        var info = {};
        for (var i = data.length - 1; i >= 0; i--) {
            info['id']              = data[i].id
            info['PrimerApellido']  = data[i].PrimerApellido
            info['SegundoApellido'] = data[i].SegundoApellido
            info['PrimerNombre']    = data[i].PrimerNombre
            info['OtroNombre']      = data[i].OtroNombre
            info['Pais']            = data[i].Pais
            info['TipoID']          = data[i].TipoID
            info['NumID']           = data[i].NumID
            info['Email']           = data[i].Email
            info['FechIng']         = data[i].FechIng
            info['Area']            = data[i].Area
            info['Estado']          = data[i].Estado
            info['FechReg']         = data[i].fecha
            informacion.push(info);
            info = {};
        }

        //Validamos informacion
        if (informacion.length > 0) {

            $scope.personas = informacion;
            $scope.$apply(); //Aplica obligatoriamente los cambios en el refresh de la pagina
            $scope.validateDataTable();

        }
    }

    //Eliminacion de registros en WebSQL
    $scope.Eliminar = function(persona) {

        debugger;
        console.log(persona.id);
        var id = persona.id;

        swal({
           title: "¿Está seguro de que desea eliminar el empleado?",
           text: "No recuperaras de nuevo este registro!",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",confirmButtonText: "Si, eliminarlo!",
           cancelButtonText: "Cancelar",
           closeOnConfirm: false,
           closeOnCancel: false }, 
        function(isConfirm){ 
           if (isConfirm) {
                db.DeleteDataReg(id);
              swal("Eliminado!", "El registro fue eliminado", "success");
           } else {
              swal("Cancelled", "El registro no se elimino", "error");
           }
        });

    }    
      
    //Funciona para validar si la tabla a mapear es DataTable y no presentar errores al usurio
    $scope.validateDataTable = function() {

         //Validamos si la tabla ya es DataTable para destuirla y reiniciarla.
         if ($.fn.DataTable.isDataTable('#tblReg')) {
             //Destruimos dataTable
             $('#tblReg').DataTable().destroy();
             //Iniciamos nuevamente la configuracion DataTable
             $scope.configDatatable();
         } else {
             //Iniciamos configuracion
             $scope.configDatatable();
         }
     }

    $scope.configDatatable = function() {

        $(document).ready(function() {
            $('#tblReg').DataTable({
                "bFilter": true,
                "dom": 'Bfrtip',
                "buttons": ['excel'],
                "scrollY": "700px", //Tamaño de sroll
                "scrollCollapse": true, //Activamos el Scroll lateral
                "scrollX": true, //Activamos el Scrol inferior

                "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "Todos"]
                ],
                "language": { //Configuracion de lenguaje
                "lengthMenu": "", //Cantidad de registros a mostrar
                "zeroRecords": "No se encontraron registros relacionados", //Texto de busqueda
                "info": "Tabla consulta empleados", //Informacion de la paginacion
                "infoEmpty": "No se encuentran registros disponibles", //
                "infoFiltered": "(Se realizo busqueda en MAX registros)", //Informacion de busqueda, si no se encuentran registros
                "searching": true,
                "search": "Busqueda",
                "paging": true,
                "paginate": { //Configuracion de botones y paginacion
                "next": "Siguiente", //Boton Siguiente
                "previous": "Anterior" //Boton Anterior
                },
                }
            });
        });
    }

    $scope.getinfo(); 


}]);


//Validaciones inputs Registro y Edicion de empleados....

function check(e) {
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}
function check1(e) {
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9_-]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}










